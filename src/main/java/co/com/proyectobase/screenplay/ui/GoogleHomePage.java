package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://www.google.com/")
public class GoogleHomePage extends PageObject {
    public static final Target APP_BUTTON = Target.the("Button that show the apps").located(By.id("gbwa"));
    public static final Target GOOGLE_TRANSLATE_BUTTON = Target.the("Google translate's button").located(By.xpath("//*[@id=\"gb51\"]"));
}
