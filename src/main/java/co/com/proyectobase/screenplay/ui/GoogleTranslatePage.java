package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class GoogleTranslatePage extends PageObject {
    public static final Target SOURCE_LANGUAGE_BUTTON = Target.the("Button for source language").located(By.xpath("/html/body/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]"));
    //public static final Target TARGET_LANGUAGE_BUTTON = Target.the("Button for target language").located(By.xpath("/html/body/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[4]/div[3]"));
    public static final Target ENGLISH_OPTION = Target.the("Option for english language").located(By.xpath("//div[@class='language-list-unfiltered-langs-sl_list']//div[@class='language_list_item_wrapper language_list_item_wrapper-en']"));
    //public static final Target SPANISH_OPTION = Target.the("Option for spanish language").located(By.xpath("//div[@class='language-list-unfiltered-langs-tl_list']//div[@class='language_list_section']/div[@class='language_list_item_wrapper language_list_item_wrapper-es item-emphasized item-selected']"));
    public static final Target TRANSLATION_TEXT_AREA = Target.the("Text area for input text").located(By.id("source"));
    public static final Target TRANSLATED_TEXT_AREA = Target.the("Text area for translated text").located(By.className("results-container"));
}
