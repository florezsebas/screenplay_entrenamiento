package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.ui.GoogleTranslatePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class TheAnswer implements Question<String> {

    public static Question is() {
        return new TheAnswer();
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(GoogleTranslatePage.TRANSLATED_TEXT_AREA).viewedBy(actor).asString();
    }


}
