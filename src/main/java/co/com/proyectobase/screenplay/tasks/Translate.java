package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.GoogleHomePage;
import co.com.proyectobase.screenplay.ui.GoogleTranslatePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Translate implements Task {

    private String word;

    public Translate(String word) {
        super();
        this.word = word;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(GoogleHomePage.APP_BUTTON));
        actor.attemptsTo(Click.on(GoogleHomePage.GOOGLE_TRANSLATE_BUTTON));
        actor.attemptsTo(Click.on(GoogleTranslatePage.SOURCE_LANGUAGE_BUTTON));
        actor.attemptsTo(Click.on(GoogleTranslatePage.ENGLISH_OPTION));
        //actor.attemptsTo(Click.on(GoogleTranslatePage.TARGET_LANGUAGE_BUTTON));
        //actor.attemptsTo(Click.on(GoogleTranslatePage.SPANISH_OPTION));
        actor.attemptsTo(Enter.theValue(word).into(GoogleTranslatePage.TRANSLATION_TEXT_AREA));
    }

    public static Translate fromEnglishToSpanish(String word) {
        return Tasks.instrumented(Translate.class, word);
    }
}
