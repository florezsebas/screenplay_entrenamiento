#Autor: dflorezg@choucairtesting.com
Feature: Google translate
  As a web user
  I want to  use Google Translate
  to translate  words between different languages

  @Translate
  Scenario: Translate from Source Language to Target Language
    Given that Sebastian want to use Google Translate
    When he translate the word table from English to Spanish
    Then he should see the word mesa on the screen