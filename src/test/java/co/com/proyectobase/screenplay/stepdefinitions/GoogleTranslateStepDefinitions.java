package co.com.proyectobase.screenplay.stepdefinitions;

import co.com.proyectobase.screenplay.questions.TheAnswer;
import co.com.proyectobase.screenplay.tasks.OpenTask;
import co.com.proyectobase.screenplay.tasks.Translate;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;

public class GoogleTranslateStepDefinitions {

    @Managed
    private WebDriver hisBrowser;
    private Actor actorsebas = Actor.named("Sebastian");

    @Before
    public void initialSetup() {
        actorsebas.can(BrowseTheWeb.with(hisBrowser));
    }

    @Given("^that Sebastian want to use Google Translate$")
    public void thatSebastianWantToUseGoogleTranslate() {
        actorsebas.wasAbleTo(OpenTask.thePageOfGoogle());
    }

    @When("^he translate the word (.*) from English to Spanish$")
    public void heTranslateTheWordTableFromEnglishToSpanish(String word) {
        actorsebas.attemptsTo(Translate.fromEnglishToSpanish(word));
    }

    @Then("^he should see the word (.*) on the screen$")
    public void heShouldSeeTheWordMesaOnTheScreen(String expectedWord) {
        actorsebas.should(seeThat(TheAnswer.is(), equalTo(expectedWord)));
    }
}
