package co.com.proyectobase.screenplay.runners;

import cucumber.api.SnippetType;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
//@CucumberOptions (features = "src/test/resources/features/", tags = "@SmokeTest")
@CucumberOptions (
        features = "src/test/resources/features/google_translate.feature",
        tags = "@Translate",
        glue = "co.com.proyectobase.screenplay.stepdefinitions",
        snippets = SnippetType.CAMELCASE)
//@CucumberOptions (features = "src/test/resources/features/Nombre.feature", tags = "@CasoAlterno")
public class RunnerTags {

}
